// ==UserScript==
// @name        Tumblr Reblog Count
// @namespace   asdjnsadjknasdkjn
// @include     http://www.tumblr.com/dashboard
// @include     http://www.tumblr.com/dashboard/
// @include     http://*.tumblr.com/post/*
// @version     1
// ==/UserScript==

var REQUEST_TIMEOUT = 5000;

function countReblogs(notes) {
    return notes.querySelectorAll('.reblog').length;
}

function getCount(url_prefix, post_id, token, callback, count, from_c) {
    var timeout;
    var req = new XMLHttpRequest();
    req.open('GET', url_prefix + '/notes/' + post_id + '/' + 
             token + (from_c ? '?from_c=' + from_c : ''), true);
    req.responseType = 'document';
    req.addEventListener('load', function(e) {
        var more_notes_link = this.response.querySelector('a.more_notes_link');
        var more_notes_onclick;
        var result;

        if (timeout) {
            clearTimeout(timeout);
        }

        count += countReblogs(this.response);

        if (more_notes_link) {
            more_notes_onclick = more_notes_link.getAttribute('onclick');
            result = more_notes_onclick.match(/\?from_c=([0-9]+)'/);

            if (result.length === 2) {
                getCount(url_prefix, post_id, token, callback, count, result[1]);
            }
        }

        callback(count, !more_notes_link);
    }, false);

    if (req.timeout !== undefined) {
        req.timeout = REQUEST_TIMEOUT;
        req.addEventListener('timeout', function(e) {
            getCount(url_prefix, post_id, token, callback, count, from_c);
        }, false);
    } else {
        timeout = setTimeout(function() {
            req.abort();
            getCount(url_prefix, post_id, token, callback, count, from_c);
        }, REQUEST_TIMEOUT);
    }

    count = count || 0;
    req.send();
}

function addDashboardCountLink(post, post_id, token) {
    var show_notes_link = post.querySelector('.reblog_count');
    var count_link = document.createElement('a');

    count_link.className = 'post_control reblog_count count_link';
    count_link.setAttribute('href', 'javascript:void(0)');
    count_link.innerHTML = '<span>RB#: ?</span>';
    count_link.addEventListener('click', function(e) {
        getCount('http://www.tumblr.com/dashboard', post_id, token, function(count, finished) {
            var countSpan = count_link.querySelector('span');
            countSpan.innerHTML = 'RB#: ' + (finished ? '' : '>=') + (count - finished);
        });
    }, true);

    show_notes_link.parentNode.insertBefore(count_link, show_notes_link.nextSibling);
}

function modifyDashboardPost(post) {
    var show_notes_link = post.querySelector('.reblog_count');
    var show_notes_onclick = show_notes_link.getAttribute('onclick');
    var result = show_notes_onclick.match(/^display_post_notes\(([0-9]+), '([^']+)'\)/);
    var post_id;
    var token;

    if (result.length === 3) {
        post_id = result[1];
        token = result[2];

        addDashboardCountLink(post, post_id, token);
    }
}

function modifyDashboardPosts(posts) {
    var forEach = Array.prototype.forEach;
    forEach.call(posts, modifyDashboardPost);
}

function modifyExistingDashboardPosts() {
    var posts = document.querySelectorAll('#posts > li.post[id^=post_]');

    modifyDashboardPosts(posts);
}

function registerDashboardObserver() {
    var filter = Array.prototype.filter;
    var target = document.querySelector('#posts');
    var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
    var observer = new MutationObserver(function(mutations) {
      mutations.forEach(function(mutation) {
        modifyDashboardPosts(filter.call(mutation.addedNodes, function(n) { return n.nodeName === 'LI' }));
      });
    });
    var options = { childList: true };

    observer.observe(target, options);
}

function modifyPostNotesList() {
    var notes = document.querySelector('ol.notes');
    var count_li = document.createElement('li');
    var more_notes_link;
    var count_link;

    count_li.className = 'note';

    if (notes) {
        more_notes_link = notes.querySelector('a.more_notes_link');

        if (more_notes_link) {
            count_li.innerHTML = '<a href="javascript:void(0)">Total reblogs: ?</a>';
            count_link = count_li.querySelector('a');
            count_link.addEventListener('click', function() {
                var url_prefix = document.location.href.match(/^http:\/\/[^.]+\.tumblr\.com/)[0];
                var more_notes_onclick;
                var result;
                var post_id;
                var token;

                more_notes_onclick = more_notes_link.getAttribute('onclick');
                result = more_notes_onclick.match(/([0-9]+)\/([^?]+)\?from_c/);

                if (result.length === 3) {
                    post_id = result[1];
                    token = result[2];

                    getCount(url_prefix, post_id, token, function(count, finished) {
                        count_link.innerHTML = 'Total reblogs: ' + (finished ? '' : '>=') + (count - finished);
                    }, countReblogs(notes) - 1);
                }
            });
        } else {
            count_li.innerHTML = 'Total reblogs: ' + (countReblogs(notes) - 1);
        }

        notes.appendChild(count_li);
    }
}

if (document.location.href.indexOf('http://www.tumblr.com/') === 0) {
    modifyExistingDashboardPosts();
    registerDashboardObserver();
} else {
    modifyPostNotesList();
}
